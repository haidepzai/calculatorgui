package de.hdm_stuttgart.mi.sd;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.util.ArrayList;
import java.awt.Color;

public class CalculatorGUI {

    private JFrame frame;
    private JTextField textField;

    private double num1;
    private double num2;
    private double result;
    private String operation = "";
    ArrayList<Double> storageNums = new ArrayList<Double>(); //Um mehrere Zahlen miteinander zu addieren

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                CalculatorGUI window = new CalculatorGUI();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the application.
     */
    public CalculatorGUI() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame("Hai-Rechner");
        frame.setBounds(100, 100, 252, 415);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        textField = new JTextField();
        textField.setFont(new Font("Tahoma", Font.BOLD, 16));
        textField.setBounds(10, 11, 213, 50);
        frame.getContentPane().add(textField);
        textField.setColumns(10);


//-----------------------1. Reihe----------------------------------------------------------------

        JButton btnDel = new JButton("<-");
        btnDel.addActionListener(e -> {

            String newNum;

            if (textField.getText().length() > 0) {
                StringBuilder str = new StringBuilder(textField.getText());
                str.deleteCharAt(textField.getText().length() - 1); //Letztes Zechen löschen
                newNum = str.toString();
                textField.setText(newNum);
            }
        });
        btnDel.setBackground(new Color(0, 153, 255));
        btnDel.setOpaque(true);
        btnDel.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnDel.setBounds(10, 72, 50, 50);
        frame.getContentPane().add(btnDel);

        JButton btnReset = new JButton("C");
        btnReset.addActionListener(e -> {
            textField.setText(null);
            num1 = 0;
            num2 = 0;
            result = 0;
            operation = "";
            storageNums.clear();
        });
        btnReset.setBackground(new Color(0, 153, 255));
        btnReset.setOpaque(true);
        btnReset.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnReset.setBounds(63, 72, 50, 50);
        frame.getContentPane().add(btnReset);

        JButton btnProzent = new JButton("%");
        btnProzent.addActionListener(e -> {
            num1 = Double.parseDouble(textField.getText());
            textField.setText("");
            operation = "%";
        });
        btnProzent.setBackground(new Color(0, 153, 255));
        btnProzent.setOpaque(true);
        btnProzent.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnProzent.setBounds(115, 72, 50, 50);
        frame.getContentPane().add(btnProzent);

        JButton btnDivide = new JButton("/");
        btnDivide.addActionListener(arg0 -> {
            num1 = Double.parseDouble(textField.getText());
            textField.setText("");
            operation = "/";
        });
        btnDivide.setBackground(new Color(204, 102, 0));
        btnDivide.setOpaque(true);
        btnDivide.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnDivide.setBounds(175, 72, 50, 50);
        frame.getContentPane().add(btnDivide);


//-----------------------2. Reihe----------------------------------------------------------------

        JButton btn7 = new JButton("7");
        btn7.addActionListener(e -> {

            StringBuilder str7 = new StringBuilder();
            str7.append(textField.getText() + btn7.getText());
            textField.setText(str7.toString());
        });
        btn7.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn7.setBounds(10, 133, 50, 50);
        frame.getContentPane().add(btn7);

        JButton btn8 = new JButton("8");
        btn8.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn8.getText());
            textField.setText(str.toString());
        });
        btn8.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn8.setBounds(63, 133, 50, 50);
        frame.getContentPane().add(btn8);

        JButton btn9 = new JButton("9");
        btn9.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn9.getText());
            textField.setText(str.toString());
        });
        btn9.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn9.setBounds(115, 133, 50, 50);
        frame.getContentPane().add(btn9);

        JButton btnPlus = new JButton("+");
        btnPlus.addActionListener(arg0 -> {

            num1 = Double.parseDouble(textField.getText());
            storageNums.add(num1);
            textField.setText("");
            operation = "+";

        });
        btnPlus.setBackground(new Color(204, 102, 0));
        btnPlus.setOpaque(true);
        btnPlus.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnPlus.setBounds(173, 133, 50, 50);
        frame.getContentPane().add(btnPlus);

//-----------------------3. Reihe----------------------------------------------------------------

        JButton btn4 = new JButton("4");
        btn4.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn4.getText());
            textField.setText(str.toString());
        });
        btn4.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn4.setBounds(10, 194, 50, 50);
        frame.getContentPane().add(btn4);

        JButton btn5 = new JButton("5");
        btn5.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn5.getText());
            textField.setText(str.toString());
        });
        btn5.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn5.setBounds(63, 194, 50, 50);
        frame.getContentPane().add(btn5);

        JButton btn6 = new JButton("6");
        btn6.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn6.getText());
            textField.setText(str.toString());
        });
        btn6.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn6.setBounds(115, 194, 50, 50);
        frame.getContentPane().add(btn6);

        JButton btnMinus = new JButton("-");
        btnMinus.addActionListener(e -> {
            num1 = Double.parseDouble(textField.getText());
            textField.setText("");
            operation = "-";
        });
        btnMinus.setBackground(new Color(204, 102, 0));
        btnMinus.setOpaque(true);
        btnMinus.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnMinus.setBounds(173, 194, 50, 50);
        frame.getContentPane().add(btnMinus);

//-----------------------4. Reihe----------------------------------------------------------------

        JButton btn1 = new JButton("1");
        btn1.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn1.getText());
            textField.setText(str.toString());
        });
        btn1.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn1.setBounds(10, 255, 50, 50);
        frame.getContentPane().add(btn1);

        JButton btn2 = new JButton("2");
        btn2.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn2.getText());
            textField.setText(str.toString());
        });
        btn2.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn2.setBounds(63, 255, 50, 50);
        frame.getContentPane().add(btn2);

        JButton btn3 = new JButton("3");
        btn3.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn3.getText());
            textField.setText(str.toString());
        });
        btn3.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn3.setBounds(115, 255, 50, 50);
        frame.getContentPane().add(btn3);

        JButton btnMult = new JButton("*");
        btnMult.addActionListener(e -> {
            num1 = Double.parseDouble(textField.getText());
            textField.setText("");
            operation = "*";
        });
        btnMult.setBackground(new Color(204, 102, 0));
        btnMult.setOpaque(true);
        btnMult.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnMult.setBounds(173, 255, 50, 50);
        frame.getContentPane().add(btnMult);

//-----------------------5. Reihe----------------------------------------------------------------

        JButton btn0 = new JButton("0");
        btn0.addActionListener(e -> {
            StringBuilder str = new StringBuilder();
            str.append(textField.getText() + btn0.getText());
            textField.setText(str.toString());
        });
        btn0.setFont(new Font("Tahoma", Font.BOLD, 20));
        btn0.setBounds(63, 316, 50, 50);
        frame.getContentPane().add(btn0);

        JButton btnDot = new JButton(".");
        btnDot.addActionListener(e -> {
            //Nur 1 Komma erlaubt
            if (!textField.getText().contains(",")) {
                StringBuilder str = new StringBuilder();
                str.append(textField.getText() + btnDot.getText());
                textField.setText(str.toString());
            }
        });
        btnDot.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnDot.setBounds(115, 316, 50, 50);
        frame.getContentPane().add(btnDot);

        JButton btnPM = new JButton("+/-");
        btnPM.addActionListener(e -> {
            double num = Double.parseDouble(String.valueOf(textField.getText()));
            num = num * (-1);
            textField.setText(String.valueOf(num));
        });
        btnPM.setFont(new Font("Tahoma", Font.BOLD, 9));
        btnPM.setBounds(10, 316, 50, 50);
        frame.getContentPane().add(btnPM);

        JButton btnEqual = new JButton("=");
        btnEqual.setBackground(new Color(0, 51, 153));
        btnEqual.setOpaque(true);
        btnEqual.addActionListener(e -> {

            String answer;
            num2 = Double.parseDouble(textField.getText());
            double num3 = storageNums.stream().mapToDouble(Double::doubleValue).sum();

            switch (operation) {
                case "+":
                    result = num3 + num2;
                    answer = String.format("%.2f", result);
                    answer = answer.replaceAll(",", ".");
                    textField.setText(answer);
                    break;
                case "-":
                    result = num1 - num2;
                    answer = String.format("%.2f", result);
                    answer = answer.replaceAll(",", ".");
                    textField.setText(answer);
                    break;
                case "*":
                    result = num1 * num2;
                    answer = String.format("%.2f", result);
                    answer = answer.replaceAll(",", ".");
                    textField.setText(answer);
                    break;
                case "/":
                    result = num1 / num2;
                    answer = String.format("%.2f", result);
                    answer = answer.replaceAll(",", ".");
                    textField.setText(answer);
                    break;
                case "%":
                    result = num1 % num2;
                    answer = String.format("%.2f", result);
                    answer = answer.replaceAll(",", ".");
                    textField.setText(answer);
                    break;

            }

            storageNums.clear(); //Liste nochmal löschen, sonst wird die Liste nochmal dazu gerechnet (Bei der Addition)


        });
        btnEqual.setForeground(new Color(255, 255, 255));
        btnEqual.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnEqual.setBounds(173, 316, 50, 50);
        frame.getContentPane().add(btnEqual);

    }
}
