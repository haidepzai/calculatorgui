# Calculator

> With Java Swing GUI

## Function

> A simple Calculator which is able to perform calculation with integer and floating numbers with common operators like + - ÷ × plus a modulo operator (division with remainder) %

## Screenshot

| <a href="https://gitlab.com/haidepzai/tic-tac-toe-gui" target="_blank">**Calculator GUI**</a>


| [![Hai](https://i.ibb.co/wwHvv5V/Calculator.jpg)](https://gitlab.com/haidepzai/calculatorgui)